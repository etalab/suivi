# Suivi Etalab

- [Charte du G8 pour l'Ouverture des Données Publiques](charte-g8-ouverture-donnees-publiques)
  _[[suivi](https://git.framasoft.org/etalab/suivi/milestones/3)]_
- [Plan d’Action National pour un Gouvernement Ouvert 2015 – 2017](https://git.framasoft.org/etalab/plan-ogp-2015-2017/)
  _[[suivi](https://git.framasoft.org/etalab/suivi/milestones/2)]_
