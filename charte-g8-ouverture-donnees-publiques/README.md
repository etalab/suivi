# Charte du G8 pour l’Ouverture des Données Publiques

## Charte du G8 pour l’Ouverture des Données Publiques

En **juin 2013**, lors du Sommet G8 de Lough Erne, les chefs d’Etat ont signé la [Charte du G8 pour l’ouverture des données publiques] (http://www.modernisation.gouv.fr/sites/default/files/fichiers-attaches/charte-g8-ouverture-donnees-publiques-fr.pdf) et se sont engagés à élaborer un Plan d’action avant 2013 dont la mise en œuvre serait évaluée en 2015.

En **novembre 2013**, le [Plan d’action pour la France] (http://fr.slideshare.net/Etalab/g8-plan-daction-open-data-pour-la-france) a été publié. Le **7 et 8 juin 2015**, le sommet du G7 sous présidence allemande sera probablement l’occasion de cette évaluation.

En amont, des organisations de la société civile (cf. point 3) publient des classements comparant l’état d’avancée des différents pays au regard de ces engagements. 

## Engagements français dans la Charte G8 pour l’Ouverture des Données Publiques

- **Engagements relatifs à la Charte** 
    
    En signant la Charte, la France s’est engagée à mettre en œuvre **5 principes** : 
    1. Données ouvertes par défaut
    2. De qualité et en quantité
    3. Accessibles et réutilisables par tous
    4. Ouvrir les données pour améliorer la gouvernance
    5. Ouvrir les données pour encourager l’innovation

- **Engagements relatifs au Plan d’action pour la France**

    Dans le Plan d’action pour la France, plusieurs engagements ont été pris en matière d’ouverture de données publiques:

    :zap: [Suivre l'avancement](https://git.framasoft.org/etalab/suivi/milestones/3)